from datetime import date, timedelta
import pickle
import easygui as gui


class Task:
    done = False

    def __init__(self, content, year, month, day):
        self.content = content
        self.date = date(year, month, day)

    def if_done(self):
        if self.done == True:
            return "Done"
        else:
            return "To do!"


def add_task():
    when = ("Today", "Tomorrow", "Day after tomorrow", "Other")
    content = gui.enterbox(msg="Enter the task... ", title="Taks")
    print("When do you want to get the task done?")
    for number, day_of_when in enumerate(when):
        print(f"{number}. {day_of_when}")
    while True:
        try:
            # select_date = int(input("Select the date: "))
            select_date = gui.indexbox(msg="When do you want to get the task done?", title="Date", choices=when)
            if select_date > len(when):
                raise ValueError
        except:
            print("Must be a number from 0 to 3")
            continue

        if select_date < len(when):
            if select_date == 0:
                today = date.today()
                year = today.year
                month = today.month
                day = today.day
            elif select_date == 1:
                tomorrow = date.today() + timedelta(days=1)
                year = tomorrow.year
                month = tomorrow.month
                day = tomorrow.day
            elif select_date == 2:
                day_after_tomorrow = date.today() + timedelta(days=2)
                year = day_after_tomorrow.year
                month = day_after_tomorrow.month
                day = day_after_tomorrow.day
            elif select_date == 3:
                year, month, day = set_own_date()
        else:
            print("Wrong. Pick from 0 to 3.")
            continue
        break

    task = Task(content, year, month, day)
    all_tasks.append(task)
    to_do_tasks.append(task)


def delete_task():
    task_to_delete = all_tasks[enumerate_tasks(all_tasks)]
    all_tasks.remove(task_to_delete)
    if task_to_delete in to_do_tasks:
        to_do_tasks.remove(task_to_delete)
    if task_to_delete in done_tasks:
        done_tasks.remove(task_to_delete)


def display(tasks):
    if len(to_do_tasks) == 0:
        print("No tasks to handle")
    for task in tasks:
        print(f"({task.if_done()}) {task.content} - {task.date}")


def set_date():
    task_to_set_date = to_do_tasks[enumerate_tasks(to_do_tasks)]
    year, month, day = set_own_date()
    task_to_set_date.date = date(year, month, day)


def get_done():
    if len(to_do_tasks) == 0:
        print("No tasks to handle")
    else:
        task_to_get_done = to_do_tasks[enumerate_tasks(to_do_tasks)]
        task_to_get_done.done = True
        done_tasks.append(task_to_get_done)
        to_do_tasks.remove(task_to_get_done)


def get_not_done():
    if len(done_tasks) == 0:
        print("Nothing to change")
    else:
        task_to_get_not_done = done_tasks[enumerate_tasks(done_tasks)]
        task_to_get_not_done.done = False
        done_tasks.remove(task_to_get_not_done)


def help():
    commands_dict = {
        'add': 'add a new task',
        'delete': 'delete a task',
        'set': 'set the date of the taks',
        'done': 'mark as done',
        'notdone': 'mark as not done',
        'all': 'displays all tasks',
        'todo': 'displays tasks to do',
        'showdone': 'displays done tasks',
        'showfor': 'displays tasks from the selected period (day, month)',
        'help': 'displays all available commands',
        'exit': 'the program will end'
    }
    for command, desctription in commands_dict.items():
        print(f"{command} - {desctription}")


def enumerate_tasks(list_of_tasks):
    for number, task in enumerate(list_of_tasks):
        print(f"{number}. {task.content}")
    while True:
        try:
            select_task = int(input("Select the task: "))
            if select_task > len(list_of_tasks):
                raise ValueError
        except:
            print("Wrong")
            continue
        if select_task < len(list_of_tasks):
            return select_task
        else:
            print("Wrong number.")
            continue


def set_own_date():
    while True:
        try:
            year = int(input("Enter the year: "))
            month = int(input("Enter the month: "))
            day = int(input("Enter the day: "))
            if date(year, month, day) < date.today():
                raise KeyError
        except KeyError:
            print("You have to choose a day that will be there yet")
            continue
        except ValueError:
            print("Something went wrong, try again.")
            continue
        return year, month, day


def select_period():
    when = ["Today", "Tomorrow", "Day after tomorrow", "Other"]
    while True:
        select_period = input("Select a period: [day/month]: ")
        if select_period == "day":
            for number, day_of_when in enumerate(when):
                print(f"{number}. {day_of_when}")
            while True:
                try:
                    select_date = int(input("Select the date: "))
                    if select_date > len(when):
                        raise ValueError
                except:
                    print("Must be a number")
                    continue
                if select_date == 0:
                    for task in all_tasks:
                        if date.today() == task.date:
                            print(f"({task.if_done()}) {task.content} - {task.date}")
                if select_date == 1:
                    for task in all_tasks:
                        if date.today() + timedelta(days=1) == task.date:
                            print(f"({task.if_done()}) {task.content} - {task.date}")
                if select_date == 2:
                    for task in all_tasks:
                        if date.today() + timedelta(days=2) == task.date:
                            print(f"({task.if_done()}) {task.content} - {task.date}")
                if select_date == 3:
                    while True:
                        try:
                            year = int(input("Enter the year: "))
                            month = int(input("Enter the month: "))
                            day = int(input("Enter the day: "))
                        except ValueError:
                            print("Invalid data, please try again")
                            continue
                        for task in all_tasks:
                            if date(year, month, day) == task.date:
                                print(f"({task.if_done()}) {task.content} - {task.date}")
                                break
        if select_period == 'month':
            while True:
                try:
                    select_month = int(input("Select a month [1-12]: "))
                    if select_month > 12 or select_month < 1:
                        raise ValueError
                except ValueError:
                    print("Wrong number of month, please try again.")
                    continue
                for task in all_tasks:
                    if task.date.month == select_month:
                        print(f"({task.if_done()}) {task.content} - {task.date}")
        else:
            print("Type \"day\" or \"month\"!")
            continue


all_tasks = []
to_do_tasks = []
done_tasks = []

try:
    with open("data.py", "rb") as load_data:
        all_tasks, to_do_tasks, done_tasks = pickle.load(load_data)
except FileNotFoundError:
    pass

for tasks_list in all_tasks, to_do_tasks, done_tasks:
    tasks_list.sort(key=lambda x: x.date)

while True:
    # select_action = input("Choose an action: ")
    select_action = gui.buttonbox(msg="Choose an action...", title="Actions", choices=(
    "add", "delete", "set", "done", "notdone", "all", "todo", "showdone", "showfor", "help", "exit"))
    if select_action == "add":
        add_task()
    elif select_action == "delete":
        if len(all_tasks) == 0:
            print("There is no tasks")
        else:
            delete_task()
    elif select_action == "set":
        set_date()
    elif select_action == "done":
        if len(all_tasks) == 0:
            print("There is no tasks")
        else:
            get_done()
    elif select_action == "notdone":
        if len(all_tasks) == 0:
            print("There is no tasks")
        else:
            get_not_done()
    elif select_action == "all":
        if len(all_tasks) == 0:
            print("There is no tasks")
        else:
            display(all_tasks)
    elif select_action == "todo":
        if len(all_tasks) == 0:
            print("There is no tasks")
        else:
            display(to_do_tasks)
    elif select_action == "showdone":
        if len(all_tasks) == 0:
            print("There is no tasks")
        else:
            display(done_tasks)
    elif select_action == "showfor":
        select_period()
    elif select_action == "help":
        help()
    elif select_action == "exit":
        print("Goodbye!")
        break
    else:
        print("Invalid command, type \"help\" to check all available commands.")
        continue
    with open("data.py", "wb") as save_data:
        pickle.dump((all_tasks, to_do_tasks, done_tasks), save_data)

